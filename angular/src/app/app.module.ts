import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SidenavComponent} from './sidenav/sidenav.component';
import {RouterModule} from "@angular/router";
import {MatSidenavModule} from "@angular/material/sidenav";
import {allRoutes} from './routes/routes';
import {ControlPanelComponent} from './control-panel/control-panel.component';
import {DashboardComponent} from './dashboard/dashboard.component'
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatTreeModule} from "@angular/material/tree";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PlantTypeComponent} from './dm/plant-type/plant-type.component';
import {HttpClientModule} from "@angular/common/http";
import {MatMenuModule} from "@angular/material/menu";
import {WaterPumpComponent} from './dm/water-pump/water-pump.component';
import {PlantTypeListComponent} from './dm/plant-type-list/plant-type-list.component';
import {WaterPumpListComponent} from './dm/water-pump-list/water-pump-list.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import { WateringConfigurationComponent } from './dm/watering-configuration/watering-configuration.component';
import { WateringConfigurationListComponent } from './dm/watering-configuration-list/watering-configuration-list.component';
import {MatSelectModule} from "@angular/material/select";
import {MatCheckboxModule} from "@angular/material/checkbox";

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    ControlPanelComponent,
    DashboardComponent,
    PlantTypeComponent,
    WaterPumpComponent,
    PlantTypeListComponent,
    WaterPumpListComponent,
    WateringConfigurationComponent,
    WateringConfigurationListComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(
            allRoutes
        ),
        MatSidenavModule,
        MatButtonModule,
        MatDividerModule,
        MatTreeModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        FormsModule,
        MatMenuModule,
        MatTableModule,
        MatPaginatorModule,
        MatSelectModule,
        MatCheckboxModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
