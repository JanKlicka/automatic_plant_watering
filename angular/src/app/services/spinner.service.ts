import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  @Output() spinnerEmitter = new EventEmitter();

  constructor() {
  }

  start() {
    this.spinnerEmitter.emit(true);
  }

  stop() {
    this.spinnerEmitter.emit(false);
  }

  isSpinning(): boolean {
    return !!document.getElementById("spinner")
  }

  // todo this is probably bad practice
  // spinner itself is in side nav (
  setWithoutPropagatingEvent(newStatus: any) {


  }
}
