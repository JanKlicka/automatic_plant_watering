import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {WateringConfiguration, WaterPump} from "../models/water-pump";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-watering-configuration-list',
  templateUrl: './watering-configuration-list.component.html',
  styleUrls: ['./watering-configuration-list.component.css']
})
export class WateringConfigurationListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'code', 'duration', 'times', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource<WateringConfiguration>();

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.http.get<WateringConfiguration[]>('api/watering-configuration').subscribe(
      next => {
        console.log(next);
        this.dataSource.data = next;
        this.dataSource.paginator = this.paginator;
      }, error => {
        console.log('error', error);
      }
    );
  }

  delete(id) {
    this.http.delete('api/watering-configuration/' + id)
      .subscribe(next => {
        console.log(next); this.fetchData()
      }, error => console.error(error));
  }
}
