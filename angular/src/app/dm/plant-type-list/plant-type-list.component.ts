import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {PlantType, WateringConfiguration} from "../models/water-pump";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-plant-type-list',
  templateUrl: './plant-type-list.component.html',
  styleUrls: ['./plant-type-list.component.css']
})
export class PlantTypeListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'code', 'description', 'actions'];
  dataSource = new MatTableDataSource<PlantType>();

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.http.get<PlantType[]>('api/plant-type').subscribe(
      next => {
        console.log(next);
        this.dataSource.data = next;
      }, error => {
        console.log('error', error);
      }
    );
  }

  delete(id) {
    this.http.delete('api/plant-type/' + id)
      .subscribe(next => {
        console.log(next);
        this.fetchData()
      }, error => console.error(error));
  }
}
