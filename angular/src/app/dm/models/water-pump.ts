// todo time input
// export class Time {
//   hours: number;
//   minutes: number;
//   seconds: number;
// }

export interface WaterPump {
  id: number;
  name: string;
  code: string;
  gpio_pin_number: number;
  sensor_pin: number;
  sensor_power_pin: number;
  stay_dry_for_s: number;
  description: string;
  active_watering_configuration: WateringConfiguration
}

export interface WateringConfiguration {
  id: number;
  watering_duration: number;
  code: string;
  watering_times: string[]; // todo
}

export interface PlantType {
  id: number;
  name: string;
  code: string;
  description: string;
  watering_configuration: WateringConfiguration;
}
