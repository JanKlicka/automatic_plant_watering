This project is overcomplicated which made me loose interest in it. I wanted to use this to learn Django, which I partially did. The basic feature of watering should work tho. 
# Automatic plant watering ("the farm")

This project aims to create a convenient way to manage automatic watering on raspberry pi.

# Requirements, goals and their completion
The main goal is to create a web interface, that will enable operation of the farm (varying priority)

- scheduler that will automatically water the plants
- some manual controls - eg. manually trigger watering
- some nice graphs (who doesn't like graphs)

I got my first inspiration from [this setup](https://www.hackster.io/ben-eagan/raspberry-pi-automated-plant-watering-with-website-8af2dc).
Hardware info at the bottom.


# Technologies
Because controlling raspberry pi requires python, to avoid any hassle,
 I used python to write the web server in, but boi, was it a mistake.
 You don't need web server, you can just use `BlockingScheduler` and setup the watering times manually
 with few lines of codes, but apparently I like to overdo things.

I am not experienced in python, so I do **NOT** advise to learn from my code, I don't really like python 
(mainly its lack of proper intellisense (at least in pycharm)). I tried my best but also I didn't want to waste much time.

- python - django/rest
- database - I am using some Postgres array field, so Postgres db is needed, but with a little bit of code,
it might be possible to migrate to other
    - if you do not wish a database, you will need to remove all DB calls, and setup the schedule 
    manually in `core/apps.py`
- optional: UI is written in angular - the only FE framework I know and django templating looked awful

I hope that using a database to keep track of the setup and to record history of watering
might result into some interesting graphs or galleries and it won't end up being total waste of time.

## Caveats
- if you edit watering times, rescheduling needs to be performed 
(either by restarting the server or hitting `reschedule` button in `control panel`)
- it is probably desired to run with switch `--noreload` because of the library used for scheduling `apscheduler` will be loaded twice
    because app config is loaded twice on django dev server... 
    (also reload can screw you up - if the reload is triggered during watering, the raspberry won't stop watering,
     it might be nice to add some failsafe)
- I was lazy when writing `watering_utils.py`, but the thread sleeps for the duration of watering,
  therefore if you water manually for, let's say, 60 seconds, then you would get timeout on gunicorn,
  the thread would get killed and created again, which was a problem because there isn't a fallback to turn off the gpios
  and the pump would be kept active, therefore it is **required** for manual requests, that would take longer than 30s, to
  set timeout on nginx/gunicorn bigger than the request would take (safe value could be 90 which is in my config files)
- see active issues
# Setup

## Software
- install python3, clone, cd into <repo>
- FE - `cd angular`, install npm (`sudo apt-get install npm`), install angular cli (`npm install -g @angular/cli` or sth,
you might get greeted with a nice warning that node.js version 10 isn't supported and you should update to newer node.js 
and that supported versions end with version 9. LMAO)
    - development - `start_w_proxy.bat` or `ng serve --proxy-config proxy.json`
    - production - ~~`ng build`, `cp -r dist/angular/* ../django_rest/static/`~~
        - build FE at other pc - `ng build`, `scp dist\angular\* pi@pi-controller-00:automatic_plant_watering/angular_build`
	- for some reason I had trouble running whitenoise, so as of now I setup nginx to serve static files (see nginx-farm and BE - prod section),
	  but in future I'd like to have it under whitenoise
- BE - cd `<repo>/django_rest`
    - create venv `python3 -m venv venv`, `venv/Scripts/activate.bat`, `pip install -r requirments.txt`
    - dev (while activated venv) - `python manage.py runserver` or setup pycharm
        - it would be preferable to run with --noreload` so that app config loads only once and the jobs get scheduled only once,
          also, this could be a problem if reload happens during watering as the gpio would be kept activated and water pump would keep pumping (see caveats)
          (this could be solved by adding and ugly condition into core app config but eh)
        - `0.0.0.0:8000` - optional, will make the server available on LAN 
        - if deploying dev server and having whitenoise serving static files, it might be needed to add `--nostatic` switch for static/ui files 
          to be server under `/` (this isn't a problem with nginx serving static files)
    - prod - create systemd service for gunicorn and set env variables for `DB_PSW`
        - copy `gunicorn.service` into `/etc/systemd/system/gunicorn.service` (and set proper absolute paths and `DB_PSW` env var)
        - run `sudo systemctl start gunicorn`, check `sudo systemctl status gunicorn` for errors, for load on boot `sudo systemctl enable gunicorn`
            - (`sudo systemctl daemon-reload && sudo systemctl restart gunicorn` on changing `gunicorn.service` file)
        - setup nginx - copy `nginx-farm` into `/etc/nginx/sites-available/farm` (change absolute paths), `ln -s /etc/nginx/sites-available/farm/ /etc/nginx/sites-enabled/farm`
        (`ln` possibly not needed if you copy into `sites-enabled`, not `available`), test with `sudo nginx -t`, `sudo systemctl restart nginx`,
         might need to enable to start on boot)

## Hardware
### Foreword
(disclaimer - I am not a certified electrician and I know very little about this hardware stuff,
 so proceed at your own caution and risk)
- Raspberry pi (zero w) as a server - I might do a distributed network with multiple raspis running,
but only one will run the main server and others will only wait for commands from the main one
- I bought a pair of really cheap 3V-5V water pumps from amazon to experiment with 
(powered through USB plug, not the raspi itself)
- I designed my own water tank with a hole for the pump in Blender to print on my 3d printer
(the result looks nice but the blender file is horrible, if feeling cute, might post it later, idk) 
- a relay - to separate raspi from the water pump circuit in case something goes wrong, the raspi survives
- planters (I am 3d printing some of them - eg. this [cat one](https://www.thingiverse.com/thing:2387700)
 or this [oddish one](https://www.thingiverse.com/thing:2016317))
- some tubing to connect water reservoir with the planters
- some LED lightning because at the time of creation of this project,
it is winter and only little of sunlight
- additionals: RGB lightning (cuz who doesn't like RGB), you might want to think about how to not set your house on fire
### Wiring things up
- Get a raspberry pi with GPIO pins (I soldered the GPIO pins myself, it was fun, but the soldering job is awful xD)

    <img height=300px src="https://gitlab.com/ichtil/automatic_plant_watering/-/wikis/uploads/9f37eec17889e47b75feb41aad554aac/photo_2020-02-08_19-21-48.jpg"/>
    
- I used a relay to connect the wires with raspberry, this way there are 2 circuits and if one goes bad, the other one survives
    - check `pinout` command on your raspberry to see which pin is which
    - the relay has VCC and GND pin - connect VCC to a 3V3 pin on raspi (red) and GND to GND (gray)
    - then use any non 3V3 or 5V or GND pin from raspi to connect to the relay pins that control it (yellow) 
    - send `LOW` on control pin (yellow) to switch the relay (purple) to connect water pump with power, send `HIGH` to disconnect
    - I used a 4 channel relay, there may be ones with less or more channels, but the logic should be the same
    - again, I am not an electrician, so be cautious while doing these, you don't want to burn down the house     
<img height=500px src="https://gitlab.com/ichtil/automatic_plant_watering/-/wikis/uploads/ec762228973dd545ad5192b80c9eaf23/photo_2020-02-08_19-43-01.jpg">
- I 3d printed my own water reservoir with a hole at the bottom in which I stuck the water pump 
  (it should be waterproof but it's chinese, you never know with these things)
   and sealed off the bottom with silicone, I tried using vulcanizing tape but it didn't get the job done.
    - also watch out for cats, mine took particular interest in drinking water from my water tank, instead of drinking from her own bowl, asshole

    <img height=500px src="https://gitlab.com/ichtil/automatic_plant_watering/-/wikis/uploads/581816fe155c3ce0bea73df406b4f099/photo_2020-02-08_19-21-50.jpg">
- this is how it looks after plugging the first one together
    - please notice how the pots are higher than the water tank itself - that's because of physics! 
    If you've ever seen how aquarium gets cleaned with a [gravel vacuum](https://www.amazon.com/Aquarium-Gravel-Cleaner-Priming-Bulb/dp/B01HM3SQN0),
    then that's exactly what's happened. The end of the tube would be lower than the water level, therefore after the pump
    stops pumping, the water keeps flowing out. I guess it depends on what kind of water pump you are using.
    <img src="https://gitlab.com/ichtil/automatic_plant_watering/-/wikis/uploads/232ebcd15b63134e77f7ababf4cffe18/photo_2020-02-08_19-21-52.jpg">
- here is an extra photo of my cat taking care of the plants
<img src="https://gitlab.com/ichtil/automatic_plant_watering/-/wikis/uploads/bfa304e8ec193cb53ef5d60e600a3668/photo_2020-02-08_21-57-08.jpg">

