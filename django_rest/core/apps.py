from datetime import datetime

from django.apps import AppConfig


class CoreConfig(AppConfig):
    from apscheduler.schedulers.background import BackgroundScheduler
    from core.utils.utils import get_logger

    name = 'core'
    log = get_logger()
    scheduler = BackgroundScheduler()

    def ready(self):
        self.schedule_waterings()
        pass  # don't delete

    def schedule_waterings(self):
        from core.models import WaterPump
        from core.models import WateringConfiguration
        from core.utils import watering_utils
        start = datetime.now()
        self.log.info('Rescheduling waterings.')
        self.scheduler.remove_all_jobs()

        for wp in WaterPump.objects.filter(active_watering_configuration__isnull=False):
            conf: WateringConfiguration = wp.active_watering_configuration
            for wt in conf.watering_times:
                self.log.info('Rescheduling for time %s and wp %s', wt, wp)
                self.log.info('%s %s %s', wt.hour, wt.minute, wt.second)
                self.scheduler.add_job(watering_utils.water, 'cron', hour=wt.hour, minute=wt.minute, second=wt.second, kwargs={'pump': wp})

        self.log.info('Scheduled configurations.')
        for wp in WaterPump.objects.filter(sensor_pin__isnull=False):
            self.log.info('Rescheduling sensor %s', wp)
            self.scheduler.add_job(watering_utils.water_sensor, 'interval', minutes=30, kwargs={'pump': wp, 'duration': 15})  # todo duration

        self.log.info('Scheduled sensors.')
        self.log.info('Rescheduled in %s ms', datetime.now().timestamp() - start.timestamp())
        if not self.scheduler.running:
            self.scheduler.start()
