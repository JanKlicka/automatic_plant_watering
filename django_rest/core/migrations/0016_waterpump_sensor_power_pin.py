# Generated by Django 3.0.7 on 2020-06-13 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_waterpump_stay_dry_for_s'),
    ]

    operations = [
        migrations.AddField(
            model_name='waterpump',
            name='sensor_power_pin',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
