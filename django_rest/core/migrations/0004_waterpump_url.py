# Generated by Django 3.0.2 on 2020-01-27 20:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_waterpump_gpio_pin_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='waterpump',
            name='url',
            field=models.URLField(default='localhost:80'),
        ),
    ]
