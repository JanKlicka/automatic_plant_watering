# Generated by Django 3.0.2 on 2020-01-25 17:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20200125_0235'),
    ]

    operations = [
        migrations.AddField(
            model_name='waterpump',
            name='gpio_pin_number',
            field=models.PositiveSmallIntegerField(default=26),
            preserve_default=False,
        ),
    ]
