from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter

from .controllers import PlantTypeController, WaterPumpController, WateringController, WateringConfigurationController, \
    PlantController, SensorController

router = DefaultRouter(trailing_slash=False)
router.register(r'plant', PlantController, basename='plant')
router.register(r'plant-type', PlantTypeController, basename='plant-type')
router.register(r'water-pump', WaterPumpController, basename='water-pump')
router.register(r'watering', WateringController, basename='watering-controller')
router.register(r'watering-configuration', WateringConfigurationController,
                basename='watering-configuration-controller')
router.register(r'sensor', SensorController, basename='sensor-controller')
# router.register(r'test', TestSet, basename='test')
core_url_patterns = [
    path('', include(router.urls)),
    # re_path('^test/(?P<p>[0-9]{2})', TestSet.as_view({'post':'test'}))
]
